<h1>
Librecast: IoT Software Updates over IPv6 Multicast Experiments

<h3>
Fed4fire Plus Proposal number: F4Fp-SME-COD210601-05

</hr>

## README

This repo covers the experimental set up for a series of experiments comparing multicast and unicast syncing of files for the usecase of file updates.
As accepted by the 9th SME and NGI Cascade Funding Call by Fed4fire plus.

It contains scripts and documention for setting up these experiments.


### License

This work is dual-licensed under GPL 2.0 and GPL 3.0.

`SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only`

### Website

https://librecast.net/


### IRC channel

`#librecast` on irc.libera.chat

If you have a question, please be patient. An answer might take a few hours
depending on time zones and whether anyone on the team is available at that
moment.  Feel free to raise an issue on the bug tracker.


### Documentation


The software used by these experiments are available at the following repos:

* iotupd  https://codeberg.org/librecast/iotupd
* unisync https://codeberg.org/librecast/unisync
* lcroute https://codeberg.org/librecast/lcroute
* lwmon   https://codeberg.org/librecast/lwmon

####  Installation
  
METHOD covers the installation and running of the code for experiment setup and results.
  
## Questions, Bug reports, Feature Requests

New issues can be raised at:

It's okay to raise an issue to ask a question.  You can also email or ask on
IRC.


<hr />

<p>
  <a href="https://www.fed4fire.eu/demo-stories/cc/librecasttesting/">
      <img width="250" src="https://www.fed4fire.eu/wp-content/uploads/sites/10/2017/03/fed4fire_logo-e1489343254776.png" alt="Logo Fed4Fire plus : abstract logo grey circle with 3 nodes coloured red, orange, yellow" class="logocenter" />
  </a>
  <a href="https://ec.europa.eu/info/index_en/">
      <img width="250" align="right" src="https://www.fed4fire.eu/wp-content/uploads/sites/10/2017/04/eu-ch-flag.jpg" alt="Flags:EU Flag and Switzerland Flag" class="logocenter" />
  </a>
</p>
<p>
This Project was funded by <a href="https://www.fed4fire.eu/"> Fed4Fire Plus </a> which has received funding from the European Union’s Horizon 2020 research and innovation programme, which is co-funded by the European Commission and the Swiss State Secretariat for Education, Research and Innovation, under grant agreement No 732638.
</p>
